﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/Toon/Lit"
{
	Properties
	{
		[Header(Color Tone)]
		_Tint ("Tint",Color) = (0,1,1,1)

		[Header(Culling)]
		[Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull", Float) = 2 //"Back"

		[Header(Border)]
		_BorderCutoff ("Border Cutoff", Range(0,1)) = 0.25
		_BorderPower ("Border Power", Range(0,5)) = 1

		[Header(Shadow)]
		_ShadowIntensity ("Shadow Intensity", Range(0,1)) = 1
		_ShadowNear ("Shadow Near", Range(0,10)) = 0
		_ShadowFadeDist("Shadow Fade Dist.", Range(0,10)) = 5
		_ShadowScale ("Shadow Scale", Range(0.01,1)) = 1
		_ShadowPower ("Shadow Power", Range(0.01,10)) = 1.25
		_ShadowCelSize ("Shadow Cel Size", Range(1,100)) = 5
		[NoScaleOffset]_ShadowTexture("Shadow Texture", 2D) = "black" {}

		[Header(Light Diffusion)]
		_DirectionalDiffusionPower("Dir. Diff. Power", Range(0,1)) = 1
		_PointDiffusionPower("Point Diff. Power", Range(0,1)) = 1

		[Header(Specularity)]
		_SpecularPower("Specular Power", Range(0.25,10)) = 1

		[Header(Cel Shading)]
		[KeywordEnum(Floor,Ceil)] _Style("Facet Style",Int) = 0
		_CelSize ("Cel Size",Range(1,10)) = 2

		[Header(Textures)]
		[NoScaleOffset]_MainTex ("Texture", 2D) = "white" {}
		[NoScaleOffset]_EmissionTex ("Emission", 2D) = "black" {}
	}

	SubShader
	{
		Pass
		{
			
			Lighting On
			Tags {"LightMode"="ForwardBase"}
			ZTest LEqual
			Cull[_Cull]
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			#pragma multi_compile_fwdbase

			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON
			
			#include "AutoLight.cginc"
			#include "Lighting.cginc"
			#include "UnityCG.cginc"
			#include "toon.cginc"			
			ENDCG
		}

		Pass
		{
			Tags{ "LightMode" = "ForwardAdd" }
			Blend One One
			ZWrite Off
			
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment pointLightFrag
			
			// make fog work
			#pragma multi_compile_fog
			#pragma multi_compile_fwdbase

			#include "AutoLight.cginc"
			#include "Lighting.cginc"
			#include "UnityCG.cginc"
			#include "toon.cginc"			

			ENDCG
		}

		// SHADOW CASTER
		Pass{
			Tags {
				"LightMode" = "ShadowCaster"
			}

			CGPROGRAM

			#pragma target 3.0

			#pragma vertex shadowVert
			#pragma fragment shadowFrag

			#include "shadows.cginc"

			ENDCG
		}
	}

	// Fallback "VertexLit"
}
