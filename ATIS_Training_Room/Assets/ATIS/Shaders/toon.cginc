﻿#if !defined(CUSTOM_TOON_INCLUDED)
#define CUSTOM_TOON_INCLUDED
 
sampler2D _MainTex;
sampler2D _EmissionTex;
sampler2D _ShadowTexture;

float4 _MainTex_ST;
float4 _Tint;
float _SpecularPower;
float _BorderCutoff;
float _BorderPower;
int _CelSize;
int _Style;
float _DirectionalDiffusionPower;
float _PointDiffusionPower;

float _ShadowIntensity;
float _ShadowScale;
float _ShadowNear;
float _ShadowFadeDist;
float _ShadowPower;
int _ShadowCelSize;

struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
};

struct v2f
{
    float2 uv : TEXCOORD0;
    float4 worldPosition : TEXCOORD1;
    float3 normalDirection : TEXCOORD2;
    float4 screenPosition : TEXCOORD6;
    float4 pos : SV_POSITION;

    LIGHTING_COORDS(3, 4)

	UNITY_FOG_COORDS(5)
};

v2f vert(appdata v)
{
    v2f o;
    o.pos = UnityObjectToClipPos(v.vertex);

    o.screenPosition = ComputeScreenPos(o.pos);

    o.worldPosition = mul(unity_ObjectToWorld, v.vertex);
    o.normalDirection = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
    UNITY_TRANSFER_FOG(o, o.vertex);

    TRANSFER_VERTEX_TO_FRAGMENT(o);
    return o;
}

fixed4 pointLightFrag(v2f i) : SV_Target
{
    float3 normalDirection = i.normalDirection;
    float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.worldPosition.xyz);

    float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.worldPosition.xyz;
    float3 lightDirection = normalize(fragmentToLightSource);
    float lightSourceDistance = length(fragmentToLightSource);

    float attenuation = 1.0 / lightSourceDistance;

    // LIGHTING
    float diffusionAmmount = attenuation * saturate(dot(normalDirection, lightDirection));

    diffusionAmmount = pow(diffusionAmmount, _PointDiffusionPower);
				
    float3 diffuseReflection = diffusionAmmount * _LightColor0.rgb;

    float3 specularReflection = diffuseReflection * pow(saturate(dot(reflect(-lightDirection, normalDirection), viewDirection)), _SpecularPower);

	// FINAL LIGHT
    float3 lightFinal = diffuseReflection + specularReflection;
				
	// SAMPLE THE TEXTURE
    fixed4 col = tex2D(_MainTex, i.uv);

    col *= _Tint;

	// APPLY FOG
    UNITY_APPLY_FOG(i.fogCoord, col);

    return float4(lightFinal, 1) * col;
}

fixed4 frag(v2f i) : SV_Target
{
    float3 normalDirection = i.normalDirection;
    float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.worldPosition.xyz);
    float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

    float attenuation = 1.0;

	// LIGHTING
    float diffusionAmmount = attenuation * saturate(dot(normalDirection, lightDirection));

    diffusionAmmount = pow(diffusionAmmount, _DirectionalDiffusionPower);

    // CHOOSE EITHER TO FLOOR OR CEIL TO FACET
    if (_Style == 0)
    {
        diffusionAmmount = floor(diffusionAmmount * _CelSize) / _CelSize;
    }
    else
    {
        diffusionAmmount = ceil(diffusionAmmount * _CelSize) / _CelSize;
    }
				
    float3 diffuseReflection = diffusionAmmount * _LightColor0.rgb;

    float3 specularReflection = diffuseReflection * pow(saturate(dot(reflect(-lightDirection, normalDirection), viewDirection)), _SpecularPower);

	// BORDER
    float border = saturate(
							dot(
								normalize(viewDirection),
								normalDirection
								)
							);

	border = pow(border, _BorderPower);

    if (border > _BorderCutoff)
    {
        border = 1;
    }
    else
    {
        border = 0;
    }

    float3 borderColor = attenuation * pow(border, _BorderPower);

	// FINAL LIGHT
    float3 lightFinal = diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.rgb;
				
	// sample the texture
    fixed4 col = tex2D(_MainTex, i.uv);

    fixed4 emissionColor = tex2D(_EmissionTex, i.uv);

    col *= _Tint;

	// APPLY FOG
    UNITY_APPLY_FOG(i.fogCoord, col);

    fixed4 finalColor = float4(lightFinal, 1) * col * float4(borderColor, 1);

    float shadow = LIGHT_ATTENUATION(i);

    //if (_Style == 0)
    //{
    //    shadow = floor(shadow * _ShadowCelSize) / _ShadowCelSize;
    //}
    //else
    //{
    //    shadow = ceil(shadow * _ShadowCelSize) / _ShadowCelSize;
    //}

    //int n = 0;

    //shadow = pow(shadow,_ShadowPower);


    // float distFromCam = distance(_WorldSpaceCameraPos.xyz, i.worldPosition.xyz);

    // float ratio = (distFromCam - _ShadowNear) / _ShadowFadeDist;

    //shadow = 1 - (1-shadow)*ratio;

    shadow = lerp(1, shadow, _ShadowIntensity);

    // shadow = 1 - (1-shadow) * (1-ratio);

    // fixed4 shadowColor = tex2D(_ShadowTexture, i.uv * _ShadowScale);

    // fixed4 shadowColor = tex2D(_ShadowTexture, i.screenPosition.xy * 0.6);
    // shadowColor = 1 - (1 - shadow) * (1 - shadowColor);

    
    // finalColor = finalColor * shadowColor + emissionColor;

    // finalColor = fixed4(1, 1, 1, 1) * shadow;
    
    // ----
    finalColor = finalColor * shadow + emissionColor;

    return finalColor;
}

#endif