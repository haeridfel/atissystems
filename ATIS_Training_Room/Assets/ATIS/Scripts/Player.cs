﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int playerPosition = 0;
    public float shiftFraction = 5f;
    public float shiftAmount = 2f;

    [Header("Duck Specifics")]
    public float duckTransitionTime = 0.25f;
    public float duckStayTime = 1f;

    [Header("Jump Specifics")]
    [Range(0.25f,2.25f)]
    public float jumpHeight = 1;

    [Range(0.01f,0.25f)]
    public float jumpDeltaFraction = 0.1f;

    [Range(1,5)]
    public float descendMultiplier = 1.5f;

    [Header("Animator")]
    public Animator animator;

    [Header("Game Object References")]
    public GameObject playerModel;
    public GameObject hoverboardModel;

    private Vector3 currentPosition;
    private Vector3 finalPosition;

    private Coroutine shiftCoroutine;

    private bool isShifting = false;

    private Vector3 initialPosition;

    private int shiftDirection = 0;

    private bool isAlive = false;

    public bool verticalLock = false;

	// Use this for initialization
	void Start () {
        initialPosition = transform.localPosition;
    }


    IEnumerator Duck()
    {
        float fraction = 0;
        float timeElapsed = 0;

        while (timeElapsed <= duckTransitionTime)
        {
            animator.SetFloat("Duck",(timeElapsed/duckTransitionTime));

            timeElapsed += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(duckStayTime);

        timeElapsed = 0;

        while (timeElapsed <= duckTransitionTime)
        {
            animator.SetFloat("Duck", 1-(timeElapsed / duckTransitionTime));

            timeElapsed += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        verticalLock = false;
    }

    IEnumerator Jump()
    {


        animator.SetBool("Jump", true);
        yield return new WaitForEndOfFrame();
        animator.SetBool("Jump", false);

        // verticalLock = false;
    }

    public void StartJumpMotion()
    {
        StartCoroutine(JumpMotion());
    }

    IEnumerator JumpMotion()
    {
        // ACTUALLY PROVIDE THE JUMP MOTION AFTER THE ANTICIPATION IS OVER
        float fraction = 0;
        Vector3 startPosition = playerModel.transform.localPosition;
        Vector3 peakPosition = startPosition + Vector3.up * jumpHeight;

        Debug.Log("Starting Ascend");

        while (fraction <= 1)
        {          
            playerModel.transform.localPosition = Vector3.Lerp(
                                                                startPosition,
                                                                peakPosition,
                                                                Mathf.Pow(fraction,2f)
                                                            );

            fraction += jumpDeltaFraction;
            fraction = (float)System.Math.Round(fraction,3);

            yield return new WaitForEndOfFrame();
        }

        Debug.Log("Ascend Complete");

        fraction = 1;

        while (fraction >= 0)
        {
            playerModel.transform.localPosition = Vector3.Lerp(
                                                                startPosition,
                                                                peakPosition,
                                                                fraction
                                                            );

            fraction -= jumpDeltaFraction * descendMultiplier;
            fraction = (float)System.Math.Round(fraction, 3);

            yield return new WaitForEndOfFrame();
        }

        Debug.Log("Descend Complete");

        yield return new WaitForEndOfFrame();

        verticalLock = false;

        Debug.Log("Unlocking Vertical Input");
    }


    public void VerticalShift(int direction)
    {
        // FIRST CHECK IF VERTICAL MOTION OPERATION IS BEING PROCESSED OR NOT
        if (!verticalLock)
        {
            verticalLock = true; // PROPER IMPLEMENTATION

            if (direction < 0) {
                // verticalLock = true;
                // DUCK
                StartCoroutine(Duck());
            }else if (direction > 0)
            {
                // JUMP
                StartCoroutine(Jump());
            }
        }
    }

    public void HorizontalShift(int direction)
    {
        shiftDirection = direction;

        if (!isShifting) {
            playerPosition += direction;

            Debug.Log("Direction: " + playerPosition);

            if (playerPosition > 1)
            {
                playerPosition = 1;
            }
            else if (playerPosition < -1)
            {
                playerPosition = -1;
            } else

            if (shiftCoroutine != null)
            {
                StopCoroutine(shiftCoroutine);
            }

            StartCoroutine(ShiftToTargetPosition());
        }
    }

    IEnumerator ShiftToTargetPosition()
    {
        isShifting = true;

        currentPosition = transform.localPosition;
        finalPosition = initialPosition + Vector3.right * playerPosition * shiftAmount;

        switch (shiftDirection)
        {
            case -1:
                // SHIFT LEFT
                animator.SetBool("ShiftLeft", true);
                break;

            case 1:
                animator.SetBool("ShiftRight", true);
                break;
        }

        yield return new WaitForEndOfFrame();
        animator.SetBool("ShiftLeft", false);
        animator.SetBool("ShiftRight", false);


        float lerpFraction = 0;

        while (lerpFraction<=1)
        {
            transform.localPosition = Vector3.Lerp(currentPosition,finalPosition,lerpFraction);

            // Debug.Log("Lerp Fraction: " + lerpFraction);

            lerpFraction += shiftFraction*Time.deltaTime;
            lerpFraction = (float)System.Math.Round(lerpFraction,4);

            yield return null;
        }

        isShifting = false;
    }
}
