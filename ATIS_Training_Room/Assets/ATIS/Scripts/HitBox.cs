﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class HitBox : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.other.name);
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log(collision.other.name);
    }
}
