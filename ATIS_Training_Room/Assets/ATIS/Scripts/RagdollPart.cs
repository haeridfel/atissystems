﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollPart : MonoBehaviour
{
    public GameObject character;

    public bool isRoot = false;

    private Vector3 tempPosition;

    private void Start()
    {
        tempPosition = transform.localPosition;
    }

    private void LateUpdate()
    {
        if (!isRoot)
        {
            transform.localPosition = tempPosition;
        }
    }
}