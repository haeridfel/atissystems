﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour {
    public GameObject followObject;

    public float locMatchFactor = 0.9f;
    public float rotMatchFactor = 0.75f;

	// USE THIS FOR INITIALIZATION
	void Start () {
		
	}
	
	// UPDATE IS CALLED ONCE PER FRAME
	void Update () {
        transform.position = Vector3.Lerp(transform.position, followObject.transform.position, locMatchFactor);
        transform.rotation = Quaternion.Lerp(transform.rotation, followObject.transform.rotation, rotMatchFactor);
	}
}
