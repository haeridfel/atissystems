﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    [Header("Health Bar")]
    public RectTransform healthbarRectTransform;
    public Image healthbarImage;
    public float healthMaxMeterPos = 0;
    public float healthMinMeterPos = -375;

    [Header("Health Colors")]
    public int healthMaxColorHue = 180;
    public int healthMinColorHue = 0;

    private float healthbarWidth;


	// Use this for initialization
	void Start () {
        SetHealthMeter(1f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHealthMeter(float healthFraction)
    {
        healthbarRectTransform.localPosition = Vector3.right * Mathf.Lerp(healthMinMeterPos, healthMaxMeterPos, healthFraction);   // THE OFFSET 26 IS A PRE-DETERMINED CONSTANT
        healthbarImage.color = Color.HSVToRGB(Mathf.Lerp(healthMinColorHue, healthMaxColorHue, healthFraction)/360f,1,1);
    }
}