using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class ThirdPersonCharacter : MonoBehaviour
{
	[SerializeField] float m_MovingTurnSpeed = 360;
	[SerializeField] float m_StationaryTurnSpeed = 180;
	[SerializeField] float m_JumpPower = 12f;
	[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
	[SerializeField] float m_RunCycleLegOffset = 0.2f; //specific to the character in sample assets, will need to be modified to work with others
	[SerializeField] float m_MoveSpeedMultiplier = 1f;
	[SerializeField] float m_AnimSpeedMultiplier = 1f;
	[SerializeField] float m_GroundCheckDistance = 0.3f;

    [Header("Layer Masks")]
    [SerializeField] LayerMask physicsCastMask;     // GENERIC LAYER MASK FOR BOTH RAY-CASTS AND SPHERE-CASTS
    [SerializeField] LayerMask groundCastMask;
    [SerializeField] LayerMask hitboxLayerMask;
    [SerializeField] LayerMask IKLayerMask;

    [Header("Specific Layers")]
    [SerializeField] LayerMask enemyLayer; // TO CHECK IF I HAVE HIT THE ENEMY COLLIDERS

    [Header("UI Manager")]
    public UIManager uiManager;

    [Header("Health")]
    public float maxHealth = 100;

    [Header("Wall Jump")]
    [Range(0.1f,2f)]
    public float minvVelocityY = 0.5f;
    [Range(0.1f,2f)]
    public float wallContactDuration = 0.5f;
    public float wallJumpForwardRayoffset = 0.35f;
    public float wallJumpDetectionHeight = 1.5f;
    public float wallJumpContactDistance = 0.5f;

    public float wallJumpReboundForce = 2;
    public float wallJumpVerticalBoost = 1;

    public float wallAttackReboundForce = 2;
    public float wallAttackVerticalBoost = 1;

    public float walljumpFailVerticalForce = 2;

    [Header("Stealth Attack")]
    [Range(0,1)]
    public float stealthAttackActionDistance = 0.5f;
    public Vector3 stealthRayOrigin = Vector3.up;

    [Header("Camera Rig")]
    public GameObject cameraPivot;

    [Header("Vertical Wall Run")]
    public float wallRunDistance = 3;
    public float wallRunSpeed = 0.25f;

    [Header("Ledge Hanging")]
    public float ledgeRayheight = 1;

    [Header("Ledge Climbing")]
    public Vector3 climbForce = Vector3.zero;
    public ForceMode climbForceMode = ForceMode.Impulse;


    [Header("Attack")]
    public float impactStrength = 50;
    public int attackStyleCount = 2;
    public float[] clearAttackDelays;
    public float attackDamage = 1;

    [Header("Avatars")]
    public Avatar steampunkAvatar;
    public Avatar cyberpunkAvatar;

    [Header("Renderer")]
    public SkinnedMeshRenderer skinnedMeshRenderer;

    [Header("Skins")]
    public Mesh steampunkSkinMesh;
    public Mesh cyberpunkSkinMesh;

    [Header("Skin Materials")]
    public Material steampunkSkinMaterial;
    public Material cyberpunkSkinMaterial;

    [Header("Others")]
    public GameObject playerBlockbox;

    [Range(0,1)]
    public float ikWeight = 1;

    [Range(0, 1)]
    public float feetYOffset = 0;

    [Range(0, 1)]
    public float velocityThreshold = 0.5f;

    [Range(0, 3)]
    public float hitReactionTime = 2.5f;

    public GameObject enemyDetector;

    Rigidbody m_Rigidbody;
	public Animator m_Animator;
	bool m_IsGrounded;
	float m_OrigGroundCheckDistance;
	const float k_Half = 0.5f;
	float m_TurnAmount;
	float m_ForwardAmount;
	Vector3 m_GroundNormal;
	float m_CapsuleHeight;
	Vector3 m_CapsuleCenter;
	CapsuleCollider m_Capsule;
	bool m_Crouching;

    private bool canStealthAttack = false;
    private CivilianCharacter civilian;

    private Vector3 closestEnemyPos;


    // CUSTOM CODE
    int[] headHitIndices = new int[] { 1, 0, 1, 0, 1, 0 };
    int[] bodyHitIndices = new int[] { 3, 2, 3, 2, 4, 4 };
    private float currentHealth;
    private int sideIndex = 1;
    public bool isAlive = true;
    private bool isAttacking = false;
    private bool isAlreadyAttacking = false; // TO PREVENT MULTIPLE HIT IN ONE COMBAT MOVE - BECAUSE THE COLLIDER CAN SWING AND HIT ANOTHER BODY PART OF THE ENEMY IN ONE MOVE

    private int currentAttackIndex = -1;
    private bool hitEnemy = false;

    private float blockTimeStamp;

    private bool wallJumpContact = false; // WHETHER IN CONTACT WITH WALL - DEPENDING ON PHYSICS RAYCAST
    private bool willWallJump = false;   //WHETHER THE CHARACTER WILL ACTUALLY JUMP - DEPENDING ON THE INPUT
    private bool willWallAttack = false;
    private bool willWallRunVertical = false;
    private bool hangingFromLedge = false;
    private bool climbingLedge = false;

    void Start()
	{
        currentHealth = maxHealth;

        m_Animator = GetComponent<Animator>();
		m_Rigidbody = GetComponent<Rigidbody>();
		m_Capsule = GetComponent<CapsuleCollider>();
		m_CapsuleHeight = m_Capsule.height;
		m_CapsuleCenter = m_Capsule.center;

        playerBlockbox = GameObject.Find("PlayerBlockbox");
        ToggleBlockbox(false);

		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		m_OrigGroundCheckDistance = m_GroundCheckDistance;
	}

    public bool getLifeStatus()
    {
        return isAlive;
    }

    public float getBlockTimeStamp()
    {
        return blockTimeStamp;
    }

    public void EnableDamage()
    {
        isAttacking = true;
    }

    public void ClearDamage()
    {
        isAttacking = false;
    }

    // IK
    /*
    private void OnAnimatorIK(int layerIndex)
    {
        if (true) {

            Transform leftShin = m_Animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg);
            Transform rightShin = m_Animator.GetBoneTransform(HumanBodyBones.RightLowerLeg);

            Transform leftFoot = m_Animator.GetBoneTransform(HumanBodyBones.LeftFoot);
            Transform rightFoot = m_Animator.GetBoneTransform(HumanBodyBones.RightFoot);

            Transform pelvis = m_Animator.GetBoneTransform(HumanBodyBones.Hips);

            Ray leftRay = new Ray(leftFoot.position + Vector3.up, Vector3.down);
            Ray rightRay = new Ray(rightFoot.position + Vector3.up, Vector3.down);

            RaycastHit leftHit;
            RaycastHit rightHit;

            if (Physics.Raycast(leftRay, out leftHit, Mathf.Infinity, IKLayerMask))
            {
                Debug.DrawRay(
                                leftRay.origin,
                                leftRay.direction.normalized * leftHit.distance,
                                Color.yellow
                            );

                m_Animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, ikWeight);
                m_Animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, ikWeight);

                Vector3 targetLocation = leftHit.point + Vector3.up * feetYOffset;

                // if ((leftFoot.position.y - targetLocation.y) < feetOffsetThreshold )
                // {
                m_Animator.SetIKPosition(AvatarIKGoal.LeftFoot, targetLocation);
                m_Animator.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.LookRotation(Vector3.ProjectOnPlane(-leftShin.forward, leftHit.normal)));
                // }
            }

            if (Physics.Raycast(rightRay, out rightHit, Mathf.Infinity, IKLayerMask))
            {
                Debug.DrawRay(
                                rightRay.origin,
                                rightRay.direction.normalized * rightHit.distance,
                                Color.yellow
                            );

                m_Animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, ikWeight);
                m_Animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, ikWeight);

                Vector3 targetLocation = rightHit.point + Vector3.up * feetYOffset;

                // if ((rightFoot.position.y - targetLocation.y) < feetOffsetThreshold)
                // {
                m_Animator.SetIKPosition(AvatarIKGoal.RightFoot, targetLocation);
                m_Animator.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.LookRotation(Vector3.ProjectOnPlane(-rightShin.forward, rightHit.normal)));
                // }
            }
        }
    }
    */

    public void ClearAttack()
    {
        m_Animator.SetBool("IsAttacking", false);
        m_Animator.SetBool("IsStealthAttacking", false);
    }

    IEnumerator SwapClotesWithCivilian()
    {
        yield return new WaitForSeconds(3.5f);

        // Undress the civilian
        civilian.Undress();


        // Swap the main character with the alternate character
        // m_Animator.avatar = steampunkAvatar;

        skinnedMeshRenderer.sharedMesh = steampunkSkinMesh;
        skinnedMeshRenderer.sharedMaterial = steampunkSkinMaterial;
    }

    public void Attack()
    {
        if (canStealthAttack)
        {
            // POSITION YOURSELF RIGHT BEHIND THE VICTIM AT A CERTAIN DISTANCE
            transform.position = civilian.transform.TransformPoint(stealthAttackActionDistance * Vector3.back);
            transform.eulerAngles = civilian.transform.eulerAngles;

            m_Animator.SetBool("IsStealthAttacking", true);
            civilian.GetKnockedOut();

            // SWAP CLOTHES AFTER CERTAIN TIME
            StartCoroutine(SwapClotesWithCivilian());
        }
        else if(!wallJumpContact) {
            // SET A NEW ATTACK-STYLE EVERY TIME
            // ATTACK-STYLE AND ATTACK-SIDE IS HANDLED BY ANIMATION EVENTS

            m_Animator.SetBool("IsAttacking", true);

            ++currentAttackIndex;

            // CLEAR THE ATTACK AFTER A CERTAIN AMOUNT OF TIME TO ENFORCE THE PLAYER TO PRESS THE ATTACK BUTTON AGAIN

            // Invoke("ClearAttack", clearAttackDelays[currentAttackIndex]);
            Invoke("ClearAttack", clearAttackDelays[0]);
        }
        else
        {
            if (!willWallAttack)
            {
                willWallAttack = true;
            }
        }
    }

    public void Block() {
        if (!wallJumpContact) {
            m_Animator.SetBool("IsBlocking", true);
            ToggleBlockbox(true);
            blockTimeStamp = Time.timeSinceLevelLoad;
            Debug.Log("Player blocked at: " + blockTimeStamp);
        }
        else
        {
            // READY FOR VERTICAL WALL RUN
            if (!willWallRunVertical)
            {
                willWallRunVertical = true;
            }
        }
    }

    public void ReleaseBlock()
    {
        m_Animator.SetBool("IsBlocking", false);
        ToggleBlockbox(false);
    }

    public void LockOn()
    {

    }

    public void LockOff()
    { }

    public void ToggleBlockbox(bool toggle)
    {
        if (playerBlockbox)
        {
            //Debug.Log("PlayerBlockbox found");
            playerBlockbox.gameObject.SetActive(toggle);
        }
    }

    public void Move(Vector3 move, bool crouch, bool jump)
	{
        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        if (move.magnitude > 1f) move.Normalize();
        move = transform.InverseTransformDirection(move);

        if (!hangingFromLedge) {
            CheckGroundStatus();
        }

        move = Vector3.ProjectOnPlane(move, m_GroundNormal);
        m_TurnAmount = Mathf.Atan2(move.x, move.z);
        m_ForwardAmount = move.z;

        // Debug.Log("Wall Jump Contact: " + wallJumpContact);

        // ApplyExtraTurnRotation(); // ORIGINALLY IMPLEMENTED HERE

        // CONTROL AND VELOCITY HANDLING IS DIFFERENT WHEN GROUNDED AND AIRBORNE:
        if (m_IsGrounded)
        {
            // HANDLE GROUNDED MOVEMENT
            ApplyExtraTurnRotation();
            HandleGroundedMovement(crouch, jump);
        }
        else if (hangingFromLedge)
        {
            // HANDLE LEDGE HANGING MOVEMENT
            if (crouch)
            {
                hangingFromLedge = false;
                m_Rigidbody.isKinematic = false;

                m_Animator.SetBool("IsHangingFromLedge", false);

                wallJumpContact = false;
            }else if(move.z > 0)
            {
                // Debug.Log("Climb Up");
                m_Animator.applyRootMotion = true;
                m_Animator.SetBool("IsClimbingLedge", true);
                climbingLedge = true;
            }

        }else
        {
            // HANDLE AIRBORNE MOVEMENT
            HandleAirborneMovement(jump);
        }

        ScaleCapsuleForCrouching(crouch);
        PreventStandingInLowHeadroom();

        // send input and other state parameters to the animator
        UpdateAnimator(move);
	}

    public void HitTaken()
    {
        Debug.Log("Hit Taken from Agent");
    }

    float hitReactionTimer = 0;
    bool hitReactionAnimating = false;
    float playerPrevPosTimer = 0;
    Vector3 playerPrevPos;
    bool isEnemyInReach = false;
    public void Update()
    {
        if (hitReactionAnimating)
        {
            if (hitReactionTimer < hitReactionTime)
            {
                hitReactionTimer += Time.deltaTime;
            }
            else
            {
                hitReactionTimer = 0;
                hitReactionAnimating = false;
                m_Animator.SetBool("IsTakingDamage", false);
            }
        }
        else
        {
            ClearTakingDamage();
        }
        CheckForEnemy();
        if ((m_Animator.GetBool("IsAttacking") || m_Animator.GetBool("IsBlocking")) && m_IsGrounded && !m_Animator.GetBool("IsTakingDamage") && isEnemyInReach)
        {

            transform.LookAt(closestEnemyPos);
        }

        if (playerPrevPosTimer < 3)
        {
            playerPrevPosTimer += 1;
        }
        else
        {
            playerPrevPosTimer = 0;
            playerPrevPos = transform.position;
        }

        /*
        if (hitEnemy)
        {
            Time.timeScale = 0.1f;
            // Adjust fixed delta time according to timescale
            // The fixed delta time will now be 0.02 frames per real-time second
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }*/
    }

    public Vector3 GetPlayerPrevPos()
    {
        return playerPrevPos;
    }

    public void AffectHealth(float healthDelta, int hitReactionIndex)
    {
        if (!hitReactionAnimating)
        {
            hitReactionAnimating = true;
            // TAKING DAMAGE
            m_Animator.SetBool("IsTakingDamage", true);
            m_Animator.SetInteger("HitReactionIndex", hitReactionIndex);
            Debug.Log("Player Health " + currentHealth);
            healthDelta = Mathf.Abs(healthDelta);
            currentHealth -= healthDelta;
        }
        

        //Debug.Log("Current Health: " + currentHealth);

        //uiManager.SetHealthMeter(currentHealth/maxHealth);
            
        // GENERIC FUNCTION FOR ADDING OR REDUCING HEALTH
        if (currentHealth <= 0)
        {
            //Debug.Log("Killed");
            // DEAD
            isAlive = false;

            m_Animator.SetBool("IsAlive",false);
        }

        //Invoke("ClearTakingDamage",0.25f);
    }

    void ClearTakingDamage()
    {
        m_Animator.SetBool("IsTakingDamage", false);
    }

	void ScaleCapsuleForCrouching(bool crouch)
	{
		if (m_IsGrounded && crouch)
		{
			if (m_Crouching) return;
			m_Capsule.height = m_Capsule.height / 2f;
			m_Capsule.center = m_Capsule.center / 2f;
			m_Crouching = true;
		}
		else
		{
			Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
			float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;

            bool sphereCastResult = Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, /*Physics.AllLayers*/ physicsCastMask.value, QueryTriggerInteraction.Ignore);

            if (sphereCastResult)
			{
				m_Crouching = true;
				return;
			}

			m_Capsule.height = m_CapsuleHeight;
			m_Capsule.center = m_CapsuleCenter;
			m_Crouching = false;
		}
	}

	void PreventStandingInLowHeadroom()
	{
		// prevent standing up in crouch-only zones
		if (!m_Crouching)
		{
			Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * k_Half, Vector3.up);
			float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * k_Half;

            bool sphereCastResult = Physics.SphereCast(crouchRay, m_Capsule.radius * k_Half, crouchRayLength, /*Physics.AllLayers*/ physicsCastMask.value, QueryTriggerInteraction.Ignore);

            // Debug.Log("Sphere Cast Status: " + sphereCastStatus);

            if (sphereCastResult)
			{
                // Debug.Log(sphereCastMask.value);
                m_Crouching = true;
			}
		}
	}

    void UpdateAnimator(Vector3 move)
	{
		// update the animator parameters
		m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
		m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
		m_Animator.SetBool("Crouch", m_Crouching);
		m_Animator.SetBool("OnGround", m_IsGrounded);

        if (!m_IsGrounded)
		{
			m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
		}

        // STEALTH ATTACK
        // IF ALKING VERY SLOWLY OR CROUCHING
        Vector3 velocity = transform.InverseTransformVector(m_Rigidbody.velocity);

        // Debug.Log("Global Velocity: " + m_Rigidbody.velocity + " | Local Velocity: " + velocity);

        if (m_Crouching || velocity.z < 0.25f)
        {
            // RAYCAST TO CHECK IF ENEMY IN VICINITY
            Ray ray = new Ray(transform.TransformPoint(stealthRayOrigin), transform.TransformDirection(Vector3.forward));

            float rayDrawDistance = 1;
            Color rayColor = Color.green;

            RaycastHit hit;

            bool rayCastStatus = Physics.Raycast(ray, out hit);

            if ( rayCastStatus && hit.collider.CompareTag("Civilian") )
            {
                civilian = hit.collider.gameObject.GetComponent<CivilianCharacter>();

                rayDrawDistance = hit.distance;
                rayColor = Color.red;

                canStealthAttack = true;

                Debug.Log("Hit: " + hit.collider.gameObject.name);
            }
            else
            {
                canStealthAttack = false;
            }

            Debug.DrawRay(
                            ray.origin,
                            ray.direction.normalized * rayDrawDistance,
                            rayColor
                        );
        }

        // calculate which leg is behind, so as to leave that leg trailing in the jump animation
        // (This code is reliant on the specific run cycle offset in our animations,
        // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
        float runCycle = Mathf.Repeat( m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1 );
		float jumpLeg = (runCycle < k_Half ? 1 : -1) * m_ForwardAmount;
		if (m_IsGrounded)
		{
			m_Animator.SetFloat("JumpLeg", jumpLeg);
		}

		// the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
		// which affects the movement speed because of the root motion.
		if (m_IsGrounded && move.magnitude > 0)
		{
			m_Animator.speed = m_AnimSpeedMultiplier;
		}
		else
		{
			// don't use that while airborne
			m_Animator.speed = 1;
		}
	}

    void HandleAirborneMovement(bool jump)
    {        
        // MODIFICATION: THE JUMP PARAMETER DECIDES HETHER THE PLAYER CAN JUMP OFF OF A WALL
        // APPLY EXTRA GRAVITY FROM MULTIPLIER:
        if (!wallJumpContact)
        {
            float verticalVelocity = m_Rigidbody.velocity.y;

            Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
            m_Rigidbody.AddForce(extraGravityForce);

            // NEEDS TO HAPPEN ALWAYS
            m_GroundCheckDistance = verticalVelocity < 0 ? m_OrigGroundCheckDistance : 0.3f;

            // CUSTOM IMPLEMENTATION
            // IF JUMPING ALMOST AT THE PEAK OF JUMP AND FACING A SURFACE THEN GET READY FOR WALL JUMP
            // CAST A RAY IN THE FORWARD DIRECTION

            Vector3 relativeSpaceVelocity = transform.InverseTransformDirection(m_Rigidbody.velocity).normalized;

            float relativeSpeedX = relativeSpaceVelocity.x;

            if (relativeSpeedX != 0)
            {
                // CREATE THE FORWARD RAY
                Ray ray = new Ray(
                                    transform.TransformPoint(Vector3.forward * wallJumpForwardRayoffset + Vector3.up * wallJumpDetectionHeight),
                                    transform.TransformDirection(Vector3.forward)
                                    );

                // CAST THE RAY FORWARD
                RaycastHit hit;
                bool hitResult = Physics.Raycast(ray, out hit, Mathf.Infinity ,physicsCastMask);
                Color rayColor = Color.white;

                float debugDrawDistance = 1f;

                // Debug.Log(hit.distance);

                if (hitResult && hit.distance < wallJumpContactDistance)
                {
                    rayColor = Color.red;
                    debugDrawDistance = hit.distance;

                    // Debug.Log("Contact: " + hit.collider.gameObject + " | Validated for wall jump");

                    wallJumpContact = true;

                    willWallRunVertical = false;
                    willWallAttack = false;
                    willWallJump = false;

                    // m_Rigidbody.AddForce(reboundForce, ForceMode.Impulse);

                    // Debug.Break();

                    StartCoroutine(WallContact(hit.normal));

                    // Debug.Break();
                }

                Debug.DrawRay(ray.origin, ray.direction.normalized * debugDrawDistance, rayColor, 5f);
                // Debug.Break();
            }
        }
        else
        {
            // THE CHARACTER IS IN CONTACT WITH WALL
            // OPEN THE WINDOW TO INITIATE THE WALL JUMP
            if (!willWallJump && jump)
            {
                // IF WILL-WLL-JUMP IS NOT SET
                // AND AT LEAST ONCE THE JUMP SIGNAL IS INITIATED WHILST IN CONTACT WITH WALL
                // SAVE THE WILL-WLL-JUMP FLAG SO THAT IT IS USED IN THE COROUTINE AFTER WIATING FOR A WHILE
                willWallJump = true;
            }
        }
	}

    IEnumerator WallContact(Vector3 surfaceNormal)
    {
        Debug.Log("Wall Contact Coroutine");
        
        // yield return new WaitForFixedUpdate();
        m_Rigidbody.isKinematic = true;
        m_Animator.SetBool("WallContact",true);

        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
        m_Rigidbody.ResetInertiaTensor();

        // ROTATE TOWARDS THE WALL
        // -----------------------
        Quaternion inverseSurfaceAngle = Quaternion.LookRotation(-surfaceNormal,Vector3.up);

        transform.rotation = inverseSurfaceAngle;

        // Debug.Break();

        // NOTE: INSTEAD OF DIRECTLY SETTING, PREFERABLY QUICKLY LERP TO THE ANGLE / QUATERNION
        // --------------------------------------

        float startTime = Time.time;

        Vector3 reactionForce = Vector3.zero;

        while (
                !willWallRunVertical    
                &&    
                !willWallAttack
                &&
                !willWallJump
                &&
                (Time.time - startTime) < wallContactDuration)
        {
            // WAIT UNTIL EITHER WALL-JUMP SIGNAL IS FOUND OR WAIT-DURATION IS OVER
            yield return new WaitForEndOfFrame();
        }

        if (willWallJump)
        {
            // APPLY THE REBOUND FORCE
            reactionForce = surfaceNormal * wallJumpReboundForce + Vector3.up * wallJumpVerticalBoost;

            // ONLY ROTATE THE PLAYER IN CASE OF A SUCCESSFUL WALL JUMP

            willWallJump = false;   // SO THAT IT CAN BE RE-CHECKED FOR THE NEXT WALL JUMP
            m_Animator.SetBool("WallContact", false);
        }else if (willWallAttack)
        {
            reactionForce = surfaceNormal * wallAttackReboundForce + Vector3.up * wallAttackVerticalBoost;

            // m_Rigidbody.AddForce(reactionForce, ForceMode.Impulse);
        }else if (willWallRunVertical)
        {
            m_Animator.SetBool("IsBlocking", true);
        }
        else
        {
            // APPLY A NOMINAL REACTION TO MAKE THINGS LOOK REAL
            reactionForce = Vector3.up * walljumpFailVerticalForce;
            // m_Rigidbody.AddForce(reactionForce, ForceMode.Impulse);

            m_Animator.SetBool("WallContact", false);
        }

        // if (hangingFromLedge)
        // {
            // Debug.Log("Hanging from ledge");
            // Debug.Break();
        // }

        if (willWallRunVertical)
        {
            // START THE COROUTINE FOR TRAVERSAL VERTICALLY
            StartCoroutine(WallRunVertical(surfaceNormal));
        }
        else if(!hangingFromLedge){
            // DISABLE THE IS-KINEMATIC FLAG
            m_Rigidbody.isKinematic = false;

            // APPLY THE CALCULATED REACTION FORCE
            m_Rigidbody.AddForce(reactionForce, ForceMode.Impulse);

            // IN EITHER CASE ROTATE THE PLAYER
            StartCoroutine(WallReboundRotation());
        }
    }

    private float SmoothFraction(float f, int n = 1)
    {
        float result = 0;

        if(f < 0.5f)
        {
            result = Mathf.Pow(2, n - 1) * Mathf.Pow(f,n);
        }
        else
        {
            result = Mathf.Pow(-2, n - 1) * Mathf.Pow(f-1,n) + 1;
        }

        return result;
    }

    float LerpZeroToZero(float f, int n = 2)
    {
        return 1 - Mathf.Pow(2 * f - 1, 2*n);
    }

    private IEnumerator WallRunVertical(Vector3 contactSurfaceNormal)
    {
        Vector3 cameraRigStartPosition = cameraPivot.transform.localPosition;
        Vector3 cameraRigMaxPosition = cameraPivot.transform.localPosition + Vector3.forward;

        // willWallRunVertical = false;

        Vector3 startPosition = transform.position;
        Vector3 targetPosition = transform.position + Vector3.up * wallRunDistance;

        float fraction = 0;

        while (willWallRunVertical && fraction <= 1)
        {
            // THE LOOP CAN TERMINATE WHEN THE WALL RUN FLAG IS RESET
            transform.position = Vector3.Lerp(
                                                startPosition,
                                                targetPosition,
                                                SmoothFraction(fraction)
                                                );


            cameraPivot.transform.localPosition = Vector3.Lerp(
                                                            cameraRigStartPosition,
                                                            cameraRigMaxPosition,
                                                            LerpZeroToZero(fraction)
                                                        );

            fraction += wallRunSpeed;
            fraction = (float)System.Math.Round(fraction,3);

            // ALSO CHECK IF A LEDGE IS PRESENT
            Ray ray = new Ray(transform.TransformPoint(Vector3.up * ledgeRayheight),transform.TransformDirection(Vector3.forward));

            RaycastHit hit;
            bool hitResult = Physics.Raycast(ray,out hit,Mathf.Infinity,physicsCastMask);

            float rayLength = 1;

            if (!hitResult || hit.distance > 0.5f)
            {
                willWallRunVertical = false;

                // Debug.Break();

                StartCoroutine(ReturnCameraToOriginal(cameraRigStartPosition));

                hangingFromLedge = true;

                m_Animator.SetBool("IsHangingFromLedge", true);
            }

            Debug.DrawRay(ray.origin,ray.direction*rayLength, Color.cyan);
            // Debug.Break();

            yield return new WaitForFixedUpdate();
        }

        // WALL JUMP COMPLETE
        // wallJumpContact = false;        
        willWallRunVertical = false;
        
        m_Animator.SetBool("IsBlocking", false);

        // m_Rigidbody.isKinematic = false;
        // m_Rigidbody.ResetInertiaTensor();
        // m_Rigidbody.velocity = Vector3.up * walljumpFailVerticalForce;

        StartCoroutine(WallContact(contactSurfaceNormal));
    }

    private IEnumerator ReturnCameraToOriginal(Vector3 camRigFinalPos)
    {
        Vector3 camRigStartPos = cameraPivot.transform.localPosition;
        float fraction = 0;

        while (fraction <= 1)
        {
            // Debug.Log("Fraction: " + fraction);

            cameraPivot.transform.localPosition = Vector3.Lerp(camRigStartPos, camRigFinalPos, fraction);

            fraction += 0.1f;
            fraction = (float)System.Math.Round(fraction,2);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator WallReboundRotation()
    {
        // Debug.Log("Wall Jump Rotation Started");

        float fraction = 0f;

        float initialRotY = transform.eulerAngles.y;
        float finalRotY = initialRotY + 180;

        if (finalRotY > 180 && finalRotY < 360)
        {
            finalRotY -= 360;
        }

        while (fraction <= 1)
        {
            transform.localEulerAngles = Vector3.up * Mathf.Lerp(initialRotY, finalRotY, fraction);

            // Debug.Log(transform.localEulerAngles);

            fraction += 0.05f;
            fraction = (float)System.Math.Round(fraction, 2);

            if (willWallAttack)
            {
                if (fraction > 0.5f) {
                    m_Animator.SetBool("IsAttacking", true);
                    m_Animator.SetBool("WallContact", false);

                    willWallAttack = false;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        wallJumpContact = false;

        m_Animator.SetBool("IsAttacking", false);

        // Debug.Log("Wall Jump Rotation Complete");
    }

	void HandleGroundedMovement(bool crouch, bool jump)
	{
		// check whether conditions are right to allow a jump:
		if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
		{
			// jump!
			m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
			m_IsGrounded = false;
			m_Animator.applyRootMotion = false;
			m_GroundCheckDistance = 0.3f;
		}
	}

	void ApplyExtraTurnRotation()
	{
		// help the character turn faster (this is in addition to root rotation in the animation)
		float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
		transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
	}

    private void OnCollisionEnter(Collision collision)
    {
        GameObject collidingObject = collision.gameObject;
        /*Debug.Log(collision.gameObject.name + " " +collision.gameObject.layer+" "+hitboxLayerMask);
        if (collidingObject.layer==hitboxLayerMask)
        {

            Debug.Log(collision.gameObject.name);
        }*/

        if (isAttacking)
        {
            // CHECK IF THE COLLIDING OBJECT LAYER IS WITHIN THE HITBOX LAYER MASK
            // USING THIS CHECK: hitboxLayerMask == (hitboxLayerMask.value | (1 << collidingObject.layer))

            if (hitboxLayerMask == (1 << collidingObject.layer))//(hitboxLayerMask.value | (1 << collidingObject.layer)))
            {
                Vector3 impulseDirection = collision.impulse.normalized;
                Vector3 collisionContactPoint = collision.contacts[0].point;
                hitEnemy = true;
                //Debug.Log("Hitbox Collision: " + collidingObject.name + " > " + collision.rigidbody.name);
                Enemy enemy = collision.rigidbody.GetComponentInParent<Enemy>();
                //Debug.Log("Hit "+enemy.name);
                //Debug.Log("Colliding object: "+ collidingObject.name);
                /*
                if (collidingObject.name=="Blockbox") 
                {
                    Debug.Log("hit blocked");
                    isAttacking = false;
                }*/
                if (enemy.getHealth() >= attackDamage)
                {
                    enemy.AffectHealth(-attackDamage, 2);
                }
                else
                {
                    // AT THIS POINT IF IT IS A HUMANOID - ACTIVATE THE HUMANOID RAGDOLL
                    RagdollPart ragdollPart = collision.rigidbody.GetComponent<RagdollPart>();
                    if (ragdollPart != null)
                    {
                        //Debug.Log("Ragdoll > Character: " + ragdollPart.character.name);
                        ragdollPart.character.GetComponent<Enemy>().EnableRagdoll();
                        collision.rigidbody.AddForceAtPosition(impactStrength*transform.TransformDirection(Vector3.forward), collisionContactPoint, ForceMode.Impulse);
                        // collision.rigidbody.AddForceAtPosition(impulseDirection * impactStrength, collisionContactPoint, ForceMode.Impulse);
                    }
                    else
                    {
                        Debug.Log("Non-Ragdoll Object");
                        collision.rigidbody.AddForceAtPosition(impulseDirection * impactStrength, collisionContactPoint, ForceMode.Impulse);
                    }

                    Debug.DrawRay(collisionContactPoint, impulseDirection * impactStrength, Color.red, 10f);

                    // Debug.Break();
                }
            }
            else
            {
                hitEnemy = false;
            }
        }
    }
    
    public void OnAnimatorMove()
	{
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (climbingLedge)
        {
            // Debug.Log("Climbing Up: " + m_Animator.rootPosition);
            transform.position = m_Animator.rootPosition;
            // transform.rotation = m_Animator.rootRotation;
        }else 

        if (m_IsGrounded && Time.deltaTime > 0)
		{
			Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

			// we preserve the existing y part of the current velocity.
			v.y = m_Rigidbody.velocity.y;
			m_Rigidbody.velocity = v;

            // Debug.Log("On Animator Move - Velocity: " + v);
		}        
	}

    public void LedgeClimbComplete()
    {
        // Debug.Break();

        hangingFromLedge = false;
        climbingLedge = false;
        wallJumpContact = false;

        m_Animator.SetBool("IsClimbingLedge",false);
        m_Animator.SetBool("IsHangingFromLedge",false);
        
        m_Animator.applyRootMotion = false;

        m_Rigidbody.isKinematic = false;

        StartCoroutine(WaitAndApplyClimbForce());
    }

    IEnumerator WaitAndApplyClimbForce()
    {
        yield return new WaitForFixedUpdate();

        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;

        m_Rigidbody.AddRelativeForce(climbForce, climbForceMode);
    }

    void CheckGroundStatus()
	{
        // Debug.Log("CheckGroundStatus");

        RaycastHit hitInfo;
        
#if UNITY_EDITOR
		// helper to visualise the ground check ray in the scene view
		Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * m_GroundCheckDistance));
#endif
		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
        // USE THE LAYER MASK
		if (
            Physics.Raycast(
                                transform.position + (Vector3.up * 0.1f),   // ORIGIN
                                Vector3.down,   // DIRECTION
                                out hitInfo,    // OUT HIT-INFO
                                m_GroundCheckDistance,   // MAX-DISTANCE
                                groundCastMask
                            )
            )
		{
			m_GroundNormal = hitInfo.normal;
			m_IsGrounded = true;
			m_Animator.applyRootMotion = true;
		}
		else
		{
			m_IsGrounded = false;
			m_GroundNormal = Vector3.up;
			m_Animator.applyRootMotion = false;
		}
	}

    void CheckForEnemy()
    {
        //Debug.Log("Checking for enemy");
        int enemyLayermask = 1 << 11;
        Collider[] enemyColliders = Physics.OverlapSphere(transform.position,1.5f,enemyLayermask);
        int i = 0;
        Vector3 targetEnemyPos;
        Vector3 front = new Vector3(0, 0, 1);
        float angleBetween;
        float smallestAngle = 0.0f;
        if (enemyColliders.Length == 0)
        {
            isEnemyInReach = false;
        }
        else
        {
            isEnemyInReach = true;
        }
        while (i < enemyColliders.Length)
        {
            targetEnemyPos = enemyColliders[i].transform.position;
            Vector3 targetPos = transform.InverseTransformVector(new Vector3(targetEnemyPos.x, 0, targetEnemyPos.z));
            angleBetween = Vector3.Angle(targetPos, front);
            if (angleBetween < smallestAngle || i==0)
            {
                smallestAngle = angleBetween;
                closestEnemyPos = targetEnemyPos;
                //Debug.Log("Closest enemy at "+ closestEnemyPos);
            }
            //Debug.Log(enemyColliders[i] + " detected at " + target_pos);
            i++;
        }
    }
}
