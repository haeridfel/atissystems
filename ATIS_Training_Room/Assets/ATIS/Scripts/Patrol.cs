﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public int[] sequence;
    bool loopPatrol = true;
    public List<GameObject> patrolNodes = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        foreach (var child in (transform.GetComponentsInChildren(typeof(Transform))))
        {
            //Debug.Log(child.gameObject.name);
            if (child.CompareTag("PatrolNode"))
            {
                //child.gameObject.GetComponent<Renderer>().enabled = false;
                patrolNodes.Add(child.gameObject);
                //Debug.Log(child.gameObject.name);
            }
        }

        int nodeCount = patrolNodes.Count;
        if (sequence.Length > 0)
        {
            for (int i = 0; i < sequence.Length; i++)
            {
                if (sequence[i] > nodeCount)
                {
                    sequence[i] = sequence[i] % nodeCount;
                }
            }
        }
        else
        {
            sequence = new int[nodeCount];
            for (int i = 0; i < nodeCount; i++)
            {
                sequence[i] = i;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
