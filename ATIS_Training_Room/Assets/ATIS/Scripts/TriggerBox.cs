﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBox : MonoBehaviour
{
    int[] headHitIndices = new int[] { 1, 0, 1, 0, 1, 0 };
    int[] bodyHitIndices = new int[] { 3, 2, 3, 2, 4, 4 };
    // Start is called before the first frame update

    Enemy enemy;

    bool perfectlyBlocked = false;

    void Start()
    {
        Transform rootParent = transform.root;
        //enemy = selfObj.GetComponent(typeof(Enemy)) as Enemy;
        enemy = GetParentEnemy(transform).GetComponent(typeof(Enemy)) as Enemy;
    }
    bool isBlocked = false;
    bool isAttackAnimationPlaying = false;
    int attackId = 0;
    int blockedAttack = -1;

    Transform parentEnemy;
    Transform GetParentEnemy(Transform objectTransform)
    {
        parentEnemy = objectTransform.parent;
        if (parentEnemy.CompareTag("Enemy"))
        {
            return parentEnemy;
        }
        GetParentEnemy(parentEnemy);
        return parentEnemy;
    }

    public void AttackAnimStarted()
    {
        attackId += 1;
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject colliderGameObject = other.gameObject;
        GameObject obj = GameObject.Find("CyberpunkHero");
        ThirdPersonCharacter character = obj.GetComponent(typeof(ThirdPersonCharacter)) as ThirdPersonCharacter;
        //ThirdPersonCharacter character = transform.root.GetComponent(typeof(ThirdPersonCharacter)) as ThirdPersonCharacter;
        
        if (colliderGameObject.tag=="PlayerHurtBox" && enemy.isAttacking && attackId != blockedAttack)
        {
            perfectlyBlocked = false;
            string colliderObjectName = colliderGameObject.transform.name;
            //Debug.Log(gameObject.name+ "'s trigger collided with " + colliderObjectName);

            int index = -1;
            if (colliderObjectName == "PlayerHeadHurtbox" && enemy.index >= 0)
            {
                index = headHitIndices[enemy.index];
            }
            else if (colliderObjectName == "PlayerBodyHurtbox" && enemy.index >= 0)
            {
                index = bodyHitIndices[enemy.index];
            }
            character.AffectHealth(-1, index);
        }
        else if (colliderGameObject.name == "PlayerBlockbox")
        {
            perfectlyBlocked = false;
            enemy.isAttacking = false;
            blockedAttack = attackId;
            if (enemy.getAttackTimeStamp() > 0 && character.getBlockTimeStamp() > enemy.getAttackTimeStamp())
            {
                Debug.Log("Block stamp: " + character.getBlockTimeStamp() + " attack stamp: " + enemy.getAttackTimeStamp());
                enemy.AffectHealth(0, 2);
                enemy.animator.SetBool("StaggerBonus", true);
                perfectlyBlocked = true;
                Debug.Log("Perfect Block!");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (perfectlyBlocked)
        {
            //Gizmos.DrawSphere(enemy.transform.position, 0.3f);
        }
    }
}
