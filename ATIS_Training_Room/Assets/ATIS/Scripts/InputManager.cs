﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class InputManager : MonoBehaviour {   
    public ThirdPersonUserControl thirdPersonUserControl;
    // public PostProcessLayer postProcesslayer;
    // public PostProcessVolume postProcessVolume;

    public void TogglePostFX()
    {
        // postProcesslayer.enabled = !postProcesslayer.enabled;
    }

    public void GamepadButtonDown(int i){
        // Debug.Log("Button Down: " + i);
        switch (i)
        {
            case 0:
                // A - JUMP
                thirdPersonUserControl.SetJump();
                break;

            case 1:
                // B - CROUCH
                thirdPersonUserControl.SetCrouch();
                break;

            case 2:
                // X - PUNCH
                thirdPersonUserControl.SetAttack();
                break;

            case 3:
                // Y - KICK
                thirdPersonUserControl.SetBlock();
                break;
        }
    }

    public void GamepadButtonUp(int i)
    {
        // Debug.Log("Button Up: " + i);
        switch (i)
        {
            case 0:
                // A - JUMP

                break;

            case 1:
                // B - CROUCH
                thirdPersonUserControl.UnsetCrouch();
                break;

            case 2:
                // X - PUNCH
                thirdPersonUserControl.UnsetAttack();
                break;

            case 3:
                // Y - KICK
                thirdPersonUserControl.UnsetBlock();
                break;
        }
    }

    
}