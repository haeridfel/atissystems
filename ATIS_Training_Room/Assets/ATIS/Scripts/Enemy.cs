﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [Header("Animtor")]
    public Animator animator;
    public string blendString = "RunAmount";
    public float hitReactionTime = 1.2f;

    [Header("Ground Check")]
    public float groundCheckDistance = 0.1f;

    [Header("Layer Masks")]
    public LayerMask physicsCastMask;

    [Header("Specifications")]
    public float maxHealth = 50;

    [Header("Scanner")]
    public GameObject scanner;
    public GameObject scannerParent;

    //[Header("Blocker")]
    //public GameObject blockBox;

    [Header("Vision Settings")]
    [Range(1, 20)]
    public uint rayCount = 1;
    public bool drawRayDebug = true;

    [Range(5, 90)]
    public float yawRange = 60;

    [Range(5, 90)]
    public float pitchRange = 90;

    [Range(10, 100)]
    public float scanDistance = 100;

    [Header("Scanner Debugging")]
    public float drawPersistTime = 1f;

    [Header("Movement")]
    public float moveSpeed;
    public float jumpForce = 1.25f;

    [Header("Target")]
    public GameObject target;
    public float proximityRadius = 1;
    public float proximityLocRadius = 0.25f;

    [Header("Combat Settings")]
    public int attackStyleCount = 5;
    public int[] hitReactionIndices;
    [Range(0, 1)]
    public float comboProbability = 0.5f;

    [Header("Patrol Settings")]
    public GameObject homeNode;
    private Patrol homePatrol;

    [Header("Ragdoll Settings")]
    public GameObject skeletonObject;
    public Rigidbody[] ragdollRigidbodies;
    public Collider[] ragdollColliders;

    [Header("Debug")]
    public bool enableRagdoll = false;

    // Merger Vars

    [Header("Perception States")]
    public Text stateText;
    public Text timeText;

    [Header("References")]
    public GameObject sensorContainer;
    public GameObject leftSensor;
    public GameObject rightSensor;
    public GameObject leftAngleObj;
    public GameObject rightAngleObj;

    public float searchAngleDelta = 5;
    public float maxSearchAngle = 60;
    public float walkMaxTimeWithoutGoal;

    public float footStep = 0.01f;

    public float safeDistance = 3f;
    public float searchDistance = 6f;
    public float searchGoalDistance = 15f;

    private bool clearLeft = true;
    private bool clearRight = true;
    private bool clearForward = true;

    Vector3 forwardHitPoint = Vector3.zero;
    Vector3 leftHitPoint = Vector3.zero;
    Vector3 rightHitPoint = Vector3.zero;

    private float searchAngle = 0;

    private Quaternion initialRotation;
    private Quaternion targetRotation;

    public float timeSinceWalk = 0;
    private string targetTag;

    private bool targetFound = false;
    private bool chosenRandomDirection = false;

    private float attackTimeStamp;

    private bool canStagger = true;
    private enum State { Walking, SearchingDirection, SearchingGoal, Turning };

    State currentState;

    enum Geometry { Flat, Concave, Convex };

    Geometry obstacleGeometry;

    // PRIVATE VARIABLES

    private NavMeshAgent navAgent;
    private float currentHealth;

    private NavMeshAgent rigidBody;
    private Vector3 groundNormal;

    private float runAmount = 0;

    private Coroutine blendCoroutine;
    private Coroutine combatDecisionCoroutine;

    private Vector3 initalEyePosition;

    private bool targetPositionKnown = false;
    private bool isSearching = false;
    private bool isBlending = false;
    private bool isChasing = false;
    private bool isIdling = false;
    private bool isPatrolling = false;

    //PUBLIC
    public bool isFighting = false;
    public bool isAttacking = false;

    //PRIVATE
    private bool isBlocking = false;

    private bool isGrounded = true;

    private Vector3 lastKnownTargetPosition = Vector3.zero;

    private bool alreadyHit = false;

    string stateDebug = "";

    Vector3 smoothDeltaPosition = Vector2.zero;
    Vector3 velocity = Vector2.zero;

    private ThirdPersonCharacter targetCharacter;

    Patrol currentPatrol;

    //Flow Debuggers
    bool a;
    bool b;
    bool c;
    bool d;
    bool e;
    bool f;
    bool g;
    bool h;

    void ResetDebuggers()
    {
        a = false;
        b = false;
        c = false;
        d = false;
        e = false;
        f = false;
        g = false;
        h = false;
    }

    public float getHealth()
    {
        return currentHealth;
    }

    public float getAttackTimeStamp()
    {
        return attackTimeStamp;
    }

    public void SetTargetPositionKnownStatus(bool status)
    {
        targetPositionKnown = status;
    }
    public bool GetTargetPositionKnownStatus()
    {
        return targetPositionKnown;
    }

    public Coroutine getCombatDecisionStatus()
    {
        return combatDecisionCoroutine;
    }

    private void Awake()
    {
        // Setup Ragdoll
        ragdollColliders = skeletonObject.transform.GetComponentsInChildren<Collider>();
        ragdollRigidbodies = skeletonObject.transform.GetComponentsInChildren<Rigidbody>();

        RagdollPart ragdollPart;

        for (int i = 0; i < ragdollColliders.Length; i++)
        {
            ragdollPart = ragdollColliders[i].gameObject.AddComponent<RagdollPart>();
            ragdollPart.character = gameObject;
            if (i == 0)
            {
                ragdollPart.isRoot = true;
            }
        }

        DisableRagdoll();
    }

    // Use this for initialization
    void Start()
    {
        currentHealth = maxHealth;
        rigidBody = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player");
        targetCharacter = target.GetComponent<ThirdPersonCharacter>();
        initalEyePosition = transform.InverseTransformPoint(scanner.transform.position);
        homePatrol = (Patrol)GetParentPatrol(homeNode.transform).GetComponent("Patrol");
        currentPatrol = homePatrol;
        rigidBody.updatePosition = false;
        if (ragdollColliders != null)
        {
            DisableRagdoll();
        }

        searchAngle = 0;
        //currentState = State.SearchingGoal;
    }

    public void Jump()
    {
        if (isGrounded)
        {
            //rigidBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    public void DisableRagdoll()
    {
        foreach (Rigidbody r in ragdollRigidbodies)
        {
            r.isKinematic = true;
            r.velocity = Vector3.zero;
            r.angularVelocity = Vector3.zero;
            r.ResetInertiaTensor();
        }
    }

    public void EnableRagdoll()
    {
        // DISABLE THE MAIN COLLIDER
        gameObject.GetComponent<Collider>().enabled = false;

        // DISABLE THE ANIMATOR
        animator.enabled = false;

        // ENABLE THE RAGDOLL COLLIDERS
        foreach (Collider c in ragdollColliders)
        {
            c.enabled = true;
        }

        foreach (Rigidbody r in ragdollRigidbodies)
        {
            r.isKinematic = false;
            //r.velocity = Vector3.zero;
            //r.angularVelocity = Vector3.zero;
            //r.ResetInertiaTensor();
        }

        StartCoroutine(DisableRagdollParts());
    }

    IEnumerator DisableRagdollParts()
    {
        yield return new WaitForSeconds(0.25f);

        foreach (Collider c in ragdollColliders)
        {
            c.GetComponent<RagdollPart>().enabled = false;
        }
    }

    bool canLoseTargetFromSight = true;
    bool canSeeWhileFighting = false;
    Vector3 playerPrevPos;
    // EVERY PHYSICS-UPDATE
    void FixedUpdate()
    {

        if (currentHealth <= 0)
        {
            return;
        }

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
            targetCharacter = target.GetComponent<ThirdPersonCharacter>();
        }

        ResetDebuggers();
        stateDebug = currentState.ToString();
        //CalculateGroundNormal();
        //rigidBody.SetDestination(lastKnownTargetPosition);
        // ALWAYS SET THE VERTICAL VELOCITY
        float yVelocity = (float)System.Math.Round(rigidBody.velocity.y, 3);

        animator.SetFloat("VerticalVelocity", yVelocity);

        animator.SetBool("IsGrounded", isGrounded);

        if (!isBlending)
        {
            RaycastHit hit;

            bool targetInSight = LookForTarget(out hit);
            Vector3 targetVector;
            if (target == null)
            {
                targetVector = Vector3.zero;
            }
            else
            {
                targetVector = target.transform.position - transform.position;
            }

            if (targetInSight && targetCharacter.isAlive)
            {
                // TARGET IS IN SIGHT
                b = true;
                targetPositionKnown = true;
                playerPrevPos = targetCharacter.GetPlayerPrevPos();
                lastKnownTargetPosition = target.transform.position;
                stopFighting();
                canLoseTargetFromSight = true;

                // If targetCharacter is within proximityRadius of this Agent
                if (targetVector.magnitude <= proximityRadius && targetCharacter.isAlive)
                {
                    canSeeWhileFighting = true;
                    startFighting();
                    // While attacking keep looking at the target
                    transform.LookAt(target.transform);
                    a = true;
                    // WHILE ATTACKING - STAND AT ONE PLACE
                    rigidBody.isStopped = true;
                    return;
                }
                else
                {
                    canSeeWhileFighting = false;
                }
            }
            else if (canSeeWhileFighting)
            {
                if (targetVector.magnitude <= proximityRadius && targetCharacter.isAlive)
                {
                    canSeeWhileFighting = true;
                    startFighting();
                    // While attacking keep looking at the target
                    transform.LookAt(target.transform);
                    a = true;
                    // WHILE ATTACKING - STAND AT ONE PLACE
                    rigidBody.isStopped = true;
                    return;
                }
                else
                {
                    canSeeWhileFighting = false;
                }
                return;
            }
            else if (!targetPositionKnown)
            {
                c = true;

                if (canLoseTargetFromSight || homePatrol == null)
                {
                    Debug.Log("canlose or homeptrl null");
                    if (canLoseTargetFromSight)
                    {
                        isIdle = true;
                        idleTime = 0;
                        canLoseTargetFromSight = false;
                    }

                    startAutoPatrol();
                }
                IdleOrPatrol();
                return;
            }

            stopFighting();
            Vector3 lastKnownTargetVector = lastKnownTargetPosition - transform.position;
            if (lastKnownTargetVector.magnitude <= proximityLocRadius)
            {
                targetPositionKnown = false;
            }
            else
            {
                isIdling = false;
                rigidBody.isStopped = false;
                isChasing = true;
                isFighting = false;

                rigidBody.SetDestination(lastKnownTargetPosition);
            }
        }
    }

    void startAutoPatrol()
    {
        Collider[] nearbyPatrolNodes = Physics.OverlapSphere(transform.position, 15, 1 << 16);
        if (nearbyPatrolNodes.Length == 0)
        {
            Debug.Log("No patrol nodes nearby " + name);
            return;
        }
        GameObject searchNode = nearbyPatrolNodes[0].gameObject;

        float headingDeviation = 999;

        NavMeshPath playerPrevPosToLastknownPos = new NavMeshPath();
        NavMeshPath lastKnownPosToNode = new NavMeshPath();
        NavMeshPath playerPrevPosToNode = new NavMeshPath();

        bool PPPToLKPPath = NavMesh.CalculatePath(playerPrevPos, lastKnownTargetPosition, NavMesh.AllAreas, playerPrevPosToLastknownPos);
        float PPPToLKPPathlength = 0;
        if (PPPToLKPPath)
        {
            PPPToLKPPathlength = GetPathLength(playerPrevPosToLastknownPos);
        }

        float shortestNodePath = 999;
        GameObject closestNode = searchNode;

        foreach (var collider in nearbyPatrolNodes)
        {
            Patrol currentNodeParent = GetParentPatrol(collider.transform).GetComponent<Patrol>();

            NavMeshPath selfToNodePath = new NavMeshPath();
            bool selfToNodePathExists = NavMesh.CalculatePath(transform.position, collider.transform.position, NavMesh.AllAreas, selfToNodePath);
            float selfToNodePathLength;
            if (selfToNodePathExists)
            {
                selfToNodePathLength = GetPathLength(selfToNodePath);
                if (selfToNodePathLength < shortestNodePath)
                {
                    shortestNodePath = selfToNodePathLength;
                    closestNode = collider.gameObject;
                }
            }

            bool LKPToNodePath = NavMesh.CalculatePath(lastKnownTargetPosition, collider.transform.position, NavMesh.AllAreas, lastKnownPosToNode);
            bool PPPToNodePath = NavMesh.CalculatePath(playerPrevPos, collider.transform.position, NavMesh.AllAreas, playerPrevPosToNode);
            float deviation = 0;

            if (PPPToLKPPath && LKPToNodePath && PPPToNodePath)
            {
                float LKPToNodePathlength = GetPathLength(lastKnownPosToNode);
                float PPPToNodePathlength = GetPathLength(playerPrevPosToNode);

                deviation = PPPToLKPPathlength + LKPToNodePathlength - PPPToNodePathlength;
            }

            if (deviation < headingDeviation)
            {
                headingDeviation = deviation;
                searchNode = collider.gameObject;
            }
        }

        Patrol searchNodeParent = GetParentPatrol(searchNode.transform).GetComponent<Patrol>();

        currentPatrol = searchNodeParent;
        int[] seq = currentPatrol.sequence;

        if (homePatrol == null)
        {
            /**if (headingDeviation != 0)
            {
                homePatrol = searchNodeParent;
                homeNode = searchNode;
                currentPatrol = homePatrol;
                Debug.Log("currentpatrol: " + homePatrol);
            }*/

            homePatrol = GetParentPatrol(closestNode.transform).GetComponent<Patrol>();
            homeNode = closestNode;
            currentPatrol = homePatrol;
            patrolSeqStartIndex = currentPatrol.patrolNodes.IndexOf(homeNode.gameObject);
        }
        else if (seq.Length > 0)
        {
            foreach (var nodeIndex in seq)
            {
                if (nodeIndex == currentPatrol.patrolNodes.IndexOf(searchNode.gameObject))
                {
                    patrolSeqStartIndex = nodeIndex;
                    Debug.Log(patrolSeqStartIndex);
                }
            }
        }
        else
        {
            patrolIndex = 0;
            patrolSeqStartIndex = searchNodeParent.patrolNodes.IndexOf(searchNode.gameObject);
        }
    }

    public static float GetPathLength(NavMeshPath path)
    {
        float length = 0.0f;

        if (path.status != NavMeshPathStatus.PathInvalid)
        {
            for (int i = 1; i < path.corners.Length; ++i)
            {
                length += Vector3.Distance(path.corners[i - 1], path.corners[i]);
            }
        }

        return length;
    }

    public float maxIdleTime = 2;
    public float maxPatrolTime = 5;
    float currentIdleLimit = 2;
    float idleTime = 0;
    float patrolTime = 0;
    bool isIdle;


    int patrolIndex = 0;
    int randomPatrolIndex = 0;
    int patrolSeqStartIndex = 0;
    void IdleOrPatrol()
    {
        if (isIdle)
        {
            if (idleTime < currentIdleLimit)
            {
                idleTime += Time.deltaTime;

                rigidBody.isStopped = true;

                isChasing = false;
                isFighting = false;
                h = true;
                animator.SetFloat("RunAmount", 0);
                //blendCoroutine = StartCoroutine(BlendToIdle2());
            }
            else
            {
                isIdle = false;
                idleTime = 0;
                currentIdleLimit = maxIdleTime;
            }
        }
        else
        {
            patrolTime += Time.deltaTime;
            int[] seq = currentPatrol.sequence;
            List<GameObject> patrolNodes = currentPatrol.patrolNodes;
            Vector3 patrolLocation;
            float nodeIdleTime = 0;
            int offsetPatrolIndex = patrolIndex + patrolSeqStartIndex;
            Debug.Log("patIndx: " + patrolIndex + " seq start indx: " + patrolSeqStartIndex);
            if (seq.Length > 0)
            {
                //Loop PatrolIndex once seq has been iterated through
                offsetPatrolIndex = UpdateOffsetPatrolIndex(seq.Length, offsetPatrolIndex);
                if (offsetPatrolIndex >= seq.Length)
                {
                    Debug.Log("initial offset: " + offsetPatrolIndex + " seq.len: " + seq.Length);
                    offsetPatrolIndex %= seq.Length;
                }
                Debug.Log("final offset: " + offsetPatrolIndex);
                int seqIndex = seq[offsetPatrolIndex];
                if ( seqIndex > patrolNodes.Count)
                {
                    seqIndex %= patrolNodes.Count;
                }
                patrolLocation = patrolNodes[seq[offsetPatrolIndex]].transform.position;
                nodeIdleTime = patrolNodes[seq[offsetPatrolIndex]].GetComponent<patrolNode>().idleTime;
            }
            else
            {
                offsetPatrolIndex = UpdateOffsetPatrolIndex(patrolNodes.Count, offsetPatrolIndex);
                patrolLocation = patrolNodes[offsetPatrolIndex].transform.position;
                nodeIdleTime = patrolNodes[offsetPatrolIndex].GetComponent<patrolNode>().idleTime;
            }
            float distToNextLoc = (patrolLocation - transform.position).magnitude;
            if (distToNextLoc > proximityLocRadius)
            {
                g = true;

                rigidBody.isStopped = false;
                isChasing = false;
                isFighting = false;
                rigidBody.SetDestination(patrolLocation);
            }
            else
            {
                patrolIndex += 1;
                isIdle = true; //Idle at nodes
                currentIdleLimit = nodeIdleTime;
            }
        }
    }

    int UpdateOffsetPatrolIndex(int limit, int offsetPatrolIndex)
    {
        if (limit > 0)
        {
            if (patrolIndex >= limit)
            {
                patrolIndex %= limit;
                // If just finished an auto patrol, revert to home patrol
                if (currentPatrol != homePatrol)
                {
                    currentPatrol = homePatrol;
                    patrolSeqStartIndex = homePatrol.patrolNodes.IndexOf(homeNode);
                }
            }
            if (offsetPatrolIndex >= limit)
            {
                // this enables the agent to start patrolling from midway of a seq, but still complete the whole seq
                offsetPatrolIndex %= limit;
            }
        }
        return offsetPatrolIndex;
    }

    Transform parentPatrol;
    Transform GetParentPatrol(Transform patrolNode)
    {
        parentPatrol = patrolNode.parent;
        if (parentPatrol.GetComponent<Patrol>())
        {
            return parentPatrol;
        }
        GetParentPatrol(parentPatrol);
        return parentPatrol;
    }

    Vector3 dest = Vector3.zero;
    public void runTo(Vector3 targetPos)
    {
        //Debug.Log("runto " + targetPos);
        Vector3 targetVector = targetPos - transform.position;
        isIdling = false;
        if (targetVector.magnitude > proximityLocRadius && !isIdling)
        {
            rigidBody.isStopped = false;
            // START CHASING
            isChasing = true;

            // RESET THE OTHER STATES
            isFighting = false;

            // Debug.Log("Start Chasing");

            // BLEND THE ANIMATION
            //blendRunningAlong(targetVector);

            // CHASE THE TARGET
            // Debug.Log("Chasing The Target");
            //moveAlong(targetVector);
            //LookStraightAndWalk(targetPos);
            //movePerceptively();
            rigidBody.SetDestination(targetPos);
            dest = targetPos;
        }
        else
        {
            if (!isPatrolling)
            {
                blendCoroutine = StartCoroutine(BlendToIdle());
            }
            else
            {
                //Patrol(patrolCueScript);
                currentPatrolIndex += 1;
            }
            //Patrol(patrolCueScript);
            //isChasing = false;
        }
    }

    public void blendRunningAlong(Vector3 targetVector)
    {
        Vector3 direction = Vector3.ProjectOnPlane(targetVector, groundNormal);

        float rotationAngleY = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

        blendCoroutine = StartCoroutine(BlendToRunning(rotationAngleY));
    }

    public void startFighting()
    {

        // AM I ALREADY ATTACKING? YES - KEEP ON ATTACKING | NO - START ATTACKING
        if (!isFighting)
        {
            // ATTACK
            isFighting = true;

            // RESET THE OTHER STATES
            isChasing = false;

            // Debug.Log("Start Attacking");

            // START THE COMBAT DECISION COROUTINE
            combatDecisionCoroutine = StartCoroutine(RandomCombatDecision());

            // SET THE ANIMATOR PARAMETER FLAG
            animator.SetBool("IsFighting", true);
        }
    }

    public void stopFighting()
    {
        if (isFighting)
        {
            isFighting = false;

            // SET THE ANIMATOR PARAMETER FLAG
            animator.SetBool("IsFighting", false);
            animator.SetBool("Combo", false);
            // STOP THE COMBAT DECISION COROUTINE
            if (combatDecisionCoroutine != null)
            {
                StopCoroutine(combatDecisionCoroutine);
            }

            // Debug.Log("Distance: " + targetVector.magnitude);
        }
    }

    // FOR NOW SET A RANDOM ACTION STYLE INDEX
    public int index = -1;
    public float comboIndex = -1;

    IEnumerator RandomCombatDecision()
    {
        while (isFighting)
        {
            //animator.SetBool("StaggerBonus", false);
            index = Random.Range(0, attackStyleCount);
            comboIndex = Random.Range(0.1f, 2);
            // Debug.Log("Attack Style: " + index);
            bool combo = false;
            if (comboIndex <= comboProbability)
            {
                combo = true;
            }

            ThirdPersonCharacter player = target.GetComponent<ThirdPersonCharacter>();
            bool isPlayerCrouching = player.m_Animator.GetBool("Crouch");
            bool isPlayerAttacking = player.m_Animator.GetBool("IsAttacking");

            if (isPlayerCrouching)
            {
                animator.SetInteger("AttackStyleIndex", 5);
            }
            else if (isPlayerAttacking)
            {
                animator.SetInteger("AttackStyleIndex", 6);
            }
            else
            {
                animator.SetInteger("AttackStyleIndex", index);
                animator.SetBool("Combo", combo);
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    public void CalculateGroundNormal()
    {
        RaycastHit hit;

        Ray ray = new Ray(transform.TransformPoint(Vector3.up * groundCheckDistance * 1f), Vector3.down);

        bool hitResult = Physics.Raycast(ray, out hit, physicsCastMask);

        isGrounded = false;

        if (hitResult)
        {
            groundNormal = hit.normal;

            // Debug.Log(hit.distance);

            if (hit.distance <= groundCheckDistance)
            {
                isGrounded = true;
            }
        }

        Debug.DrawRay(ray.origin, ray.direction * hit.distance);
    }

    private IEnumerator BlendToRunning(float rotationAngleY)
    {
        isBlending = true;

        // Debug.Log("Blending Started");

        float startAngleY = transform.eulerAngles.y;

        if (startAngleY > 180)
        {
            startAngleY -= 360;
        }

        // Debug.Log(startAngleY + " , " + rotationAngleY);

        while (runAmount < 1)
        {
            runAmount += 0.05f;
            runAmount = (float)System.Math.Round(runAmount, 2);

            transform.eulerAngles = Vector3.up * Mathf.Lerp(startAngleY, rotationAngleY, runAmount);

            animator.SetFloat(blendString, runAmount);

            yield return new WaitForEndOfFrame();
        }

        isBlending = false;

        // Debug.Log("Blending Complete");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isAttacking && !alreadyHit)
        {
            // TO PREVENT MULTIPLE COLLISIONS ON MULTIPLE HIT-BOXES PER ACTION MOVE

            alreadyHit = true;

            GameObject collidingObject = collision.contacts[0].thisCollider.gameObject;
            //Debug.Log("Colliding Object: " + collidingObject.name);

            if (
                collision.rigidbody != null
                &&
                collision.rigidbody.gameObject == target
                )
            {
                //Debug.Log("Hit");

                Vector3 contactPoint = collision.contacts[0].point;
                Vector3 impulse = collision.impulse;

                Debug.DrawRay(contactPoint, impulse.normalized * 10f, Color.red, 10f);

                int opponentReactionIndex = animator.GetInteger("AttackStyleIndex");

                target.GetComponent<ThirdPersonCharacter>().AffectHealth(-1f, opponentReactionIndex);
            }

            // TO ENABLE DAMAGE ON THE NEXT ACTION MOVE
            Invoke("ClearAlreadyHitStatus", 1f);
        }
    }

    public void EnableDamage()
    {
        //animator.SetBool("StaggerBonus", false);
        //Debug.Log("Enable Damage");
        isAttacking = true;
        attackTimeStamp = Time.timeSinceLevelLoad;
        animator.SetBool("CanStagger", false);
        Debug.Log("enemy attacked at: " + attackTimeStamp);
    }

    public void ClearDamage()
    {
        //Debug.Log("Clear Damage");
        isAttacking = false;
        animator.SetBool("CanStagger", true);
        canStagger = true;
    }

    public void ClearTakingDamage()
    {
        animator.SetBool("IsTakingDamage", false);
    }

    private void ClearAlreadyHitStatus()
    {
        Debug.Log("Already Hit - Restored To False");

        alreadyHit = false;
    }

    private IEnumerator BlendToIdle()
    {
        isBlending = true;
        isIdling = true;
        rigidBody.Stop();
        while (runAmount > 0)
        {
            runAmount -= 0.05f;
            runAmount = (float)System.Math.Round(runAmount, 2);

            animator.SetFloat(blendString, runAmount);

            yield return new WaitForEndOfFrame();
        }

        isBlending = false;
    }

    private IEnumerator BlendToIdle2()
    {
        isBlending = true;
        stopFighting();
        isIdling = true;
        rigidBody.Stop();
        while (runAmount > 0)
        {
            runAmount -= 0.05f;
            runAmount = (float)System.Math.Round(runAmount, 2);

            animator.SetFloat(blendString, runAmount);

            yield return new WaitForEndOfFrame();
        }

        isBlending = false;
    }

    public void AffectHealth(float healthDelta, int hitReactionIndex)
    {
        if (!hitReactionAnimating && !animator.GetCurrentAnimatorStateInfo(0).IsName("Blocking"))
        {
            hitReactionAnimating = true;
            // TAKING DAMAGE
            animator.SetBool("IsTakingDamage", true);
            animator.SetInteger("HitReactionIndex", hitReactionIndex);
            Debug.Log("Enemy heatlth " + currentHealth);
            healthDelta = Mathf.Abs(healthDelta);
            currentHealth -= healthDelta;
        }
        targetPositionKnown = true;
        lastKnownTargetPosition = target.transform.position;
    }

    float timeCounter = 0;
    float idleTimeCounter = 0;
    float patrolTimeCounter = 0;
    bool hitReactionAnimating = false;
    bool j = false;
    public void Update()
    {
        if (hitReactionAnimating)
        {
            if (timeCounter < hitReactionTime)
            {
                timeCounter += Time.deltaTime;
            }
            else
            {
                timeCounter = 0;
                hitReactionAnimating = false;
                animator.SetBool("IsTakingDamage", false);
            }
        }
        /**
        if (isIdling == true)
        {
            if (idleTimeCounter < 5)
            {
                idleTimeCounter += Time.deltaTime;
                isPatrolling = false;
            }
            else
            {
                idleTimeCounter = 0;
                isIdling = false;
                Patrol(patrolScript);
                j = false;
            }
        }

        if (isPatrolling == true)
        {
            if (patrolTimeCounter < 5)
            {
                patrolTimeCounter += Time.deltaTime;
            }
            else
            {
                patrolTimeCounter = 0;
                isPatrolling = false;
            }
        }*/
        dest = rigidBody.destination;
    }

    Vector3 currentPatrolLoc;
    int currentPatrolIndex = 0;
    void Patrol(Patrol cue)
    {
        j = true;
        isPatrolling = true;
        Transform cueTransf = cue.transform;
        int[] seq = cue.sequence;
        if (currentPatrolIndex >= seq.Length)
        {
            currentPatrolIndex %= seq.Length;
        }
        if (seq.Length > 0)
        {
            currentPatrolLoc = cueTransf.GetChild(seq[currentPatrolIndex]).position;
        }
        else
        {
            currentPatrolLoc = cueTransf.GetChild((int)Random.Range(0, cueTransf.childCount - 1)).position;
        }
        runTo(currentPatrolLoc);
    }

    private bool LookForTarget(out RaycastHit hitData)
    {
        // THIS IS A GENERIC METHOD FOR VISION PERCEPTION

        bool targetSighted = false;

        // CAST THE RAYS
        float pitchOffset = pitchRange / 2;
        float pitchGap = pitchRange / (rayCount - 1);

        float yawOffset = yawRange / 2;
        float yawGap = yawRange / (rayCount - 1);

        float pitch = 0;
        float yaw = 0;

        Vector3 rayDir;

        Ray scanRay;

        RaycastHit scanHit = new RaycastHit();

        bool scanHitStatus = false;

        Color rayColor = Color.yellow;

        // CAST MULTIPLE RAYS PER-LOOK DIRECTION
        for (int i = 0; i < rayCount; i++)
        {
            pitch = i * pitchGap - pitchOffset;

            for (int j = 0; j < rayCount; j++)
            {

                yaw = j * yawGap - yawOffset;

                scanner.transform.localEulerAngles = Vector3.right * pitch + Vector3.up * yaw;

                rayDir = scanner.transform.forward;

                scanRay = new Ray(scanner.transform.position, rayDir);

                scanHitStatus = Physics.Raycast(scanRay, out scanHit);

                float rayLength = scanDistance;

                if (scanHitStatus)
                {
                    rayColor = Color.green;

                    rayLength = scanHit.distance;

                    if (scanHit.collider.gameObject == target)
                    {
                        rayColor = Color.red;

                        targetSighted = true;

                        //Debug.Break();

                        break;
                    }
                }
                if (drawRayDebug)
                {
                    Debug.DrawRay(scanRay.origin, scanRay.direction.normalized * rayLength, rayColor, drawPersistTime);
                }
            }
        }

        hitData = scanHit;

        return targetSighted;
    }

    void UpdateStateText()
    {
        stateText.text = currentState.ToString();
    }

    void UpdateTimeText()
    {
        timeText.text = timeSinceWalk.ToString();
    }

    IEnumerator Turn(Quaternion rotationAngle)
    {
        float fraction = 0;
        initialRotation = transform.localRotation;
        targetRotation = rotationAngle;
        //Debug.Log("rotation Angle: " + rotationAngle);
        while (fraction <= 1)
        {
            // Debug.Log(fraction);

            transform.localRotation = Quaternion.Lerp(initialRotation, targetRotation, fraction);

            fraction += 0.1f;
            fraction = (float)System.Math.Round(fraction, 2);

            yield return null;
        }

        sensorContainer.transform.localEulerAngles = Vector3.zero;
        timeSinceWalk = 0;
        currentState = State.Walking;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(lastKnownTargetPosition + new Vector3(0, 0, 0), 0.1f);
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(playerPrevPos + new Vector3(0, 0, 0), 0.1f);

        Gizmos.color = Color.red;
        if (isAttacking)
        {
            //Gizmos.DrawSphere(transform.position, 0.2f);
        }
    }
}