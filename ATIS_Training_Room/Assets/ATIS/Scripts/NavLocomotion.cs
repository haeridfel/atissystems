﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavLocomotion : MonoBehaviour
{
    Animator animator;
    NavMeshAgent navAgent;
    Enemy enemyAgent;
    Vector3 smoothDeltaPosition = Vector3.zero;
    Vector3 velocity = Vector3.zero;
    public string blendString = "RunAmount";
    float runAmount = 0;
    bool dropDownUsed = false;
    OffMeshLinkData currentLink;
    Vector3 endPos;
    bool endTriggered;
    Vector3 startPos;
    bool startTriggered;
    public float blendProximity = 0.5f;
    public float velocityDamp = 0.01f;
    Vector3 posDiff;
    Vector3 transPos;

    // Start is called before the first frame update
    void Start()
    {
        //animation = GetComponent<Animation>();
        animator = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        enemyAgent = GetComponent<Enemy>();
        // Don’t update position automatically
        //navAgent.autoTraverseOffMeshLink = false;
        navAgent.updatePosition = false;
    }

    // Update is called once per frame
    void Update()
    {
        SyncAndMove();
        //dropDownUsed = false;
        startTriggered = false;
        endTriggered = false;
        if (navAgent.isOnOffMeshLink)
        {
            currentLink = navAgent.currentOffMeshLinkData;
            if (currentLink.linkType == OffMeshLinkType.LinkTypeDropDown)
            {
                if (!dropDownUsed)
                {
                    dropDownUsed = true;
                    navAgent.speed = navAgent.speed * velocityDamp;
                    animator.SetBool("IsJumping", true);
                    startPos = currentLink.startPos;
                    endPos = currentLink.endPos;
                    posDiff = endPos - startPos;
                    transPos = Vector3.zero;
                    //transform.position = navAgent.nextPosition;
                    //navAgent.velocity = posDiff.normalized * navAgent.speed;
                    /*
                    Vector2 startPosFlat = new Vector2(startPos.x, startPos.z);
                    Vector2 endPosFlat = new Vector2(endPos.x, endPos.z);
                    Vector2 flatDist = endPosFlat - startPosFlat;
                    Vector3 forwardVel = new Vector3(flatDist.x,startPos.y,flatDist.y);
                    */
                }
            }
        }
        if (dropDownUsed)
        {
            Vector3 startPosDist = transform.position - startPos;
            Vector3 endPosDist = transform.position - endPos;
            //transform.position = navAgent.nextPosition;
            //transPos += Vector3.forward * 0.5f;
            //transform.position = transform.InverseTransformPoint(transPos);
            if (startPosDist.magnitude < blendProximity)
            {
                startTriggered = true;
            }
            else
                startTriggered = false;
            if (endPosDist.magnitude < blendProximity)
            {
                navAgent.speed = navAgent.speed / velocityDamp;
                dropDownUsed = false;
                animator.SetBool("IsJumping", false);
                endTriggered = true;
                //navAgent.CompleteOffMeshLink();
                //navAgent.Resume();
                //animator.SetBool("IsGrounded", true);
            }
            if (!navAgent.isOnOffMeshLink)
            {
                navAgent.speed = navAgent.speed / velocityDamp;
                dropDownUsed = false;
                animator.SetBool("IsJumping", false);
                endTriggered = true;
            }
        }
    }
    float worldPosMag;
    Vector3 navNextPos;
    Vector3 Pos;
    void SyncAndMove()
    {
        Vector3 worldDeltaPosition = navAgent.nextPosition - transform.position;
        navNextPos = navAgent.nextPosition;
        Pos = transform.position;
        worldPosMag = worldDeltaPosition.magnitude;
        if (worldDeltaPosition.sqrMagnitude <= 0.00000000001)
        {
            runAmount = 0;
        }
        else if (enemyAgent.GetTargetPositionKnownStatus())
        {
            runAmount = 1;
        }
        else
        {
            runAmount = 1;
        }

        animator.SetFloat(blendString, runAmount);

        // Pull agent towards character
        /**
        if (worldDeltaPosition.magnitude > navAgent.radius)
        {
            navAgent.nextPosition = transform.position + 0.9f * worldDeltaPosition;
        }*/
    }

    void OnAnimatorMove()
    {
        transform.position = navAgent.nextPosition;
        /*
        // Update position based on animation movement using navigation surface height
        Vector3 position = animator.rootPosition;
        position.y = navAgent.nextPosition.y;
        transform.position = position;*/
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (endTriggered)
            Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(endPos, blendProximity);
        if (startTriggered)
            Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(startPos, blendProximity);
        if (navAgent)
        {
            Gizmos.DrawSphere(navAgent.nextPosition, 0.2f);
        }
        
    }
}
