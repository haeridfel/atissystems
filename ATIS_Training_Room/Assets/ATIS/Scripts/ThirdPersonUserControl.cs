using System;
using UnityEngine;
using CustomControls;

[RequireComponent(typeof (ThirdPersonCharacter))]
public class ThirdPersonUserControl : MonoBehaviour
{
    [Header("Custom Input System")]
    public CustomJoystick movementJoystick;
        
    private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
    private Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    private Vector3 m_Move;
    private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.

    private bool crouchState = false;
        
    private void Start()
    {
        // get the transform of the main camera
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }

        // get the third person character ( this should never be null due to require component )
        m_Character = GetComponent<ThirdPersonCharacter>();
    }

    public void SetJump()
    {
        if (!m_Jump) {
            m_Jump = true;
        }
    }

    public void SetCrouch()
    {
        crouchState = true;
    }

    public void UnsetCrouch()
    {
        crouchState = false;
    }

    // ATTACK
    public void SetAttack()
    {
        m_Character.Attack();
    }

    public void UnsetAttack()
    {
        m_Character.ClearAttack();
    }

    //LOCK-ON
    public void SetLockOn()
    {
        m_Character.LockOn();
    }

    public void UnsetLockOn()
    {
        m_Character.LockOff();
    }

    // BLOCK
    public void SetBlock()
    {
        m_Character.Block();
    }

    public void UnsetBlock()
    {
        m_Character.ReleaseBlock();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }

        if (m_Character.isAlive)
        {
#if UNITY_EDITOR
            if (!m_Jump)
            {
                m_Jump = Input.GetButtonDown("Jump");

                // PUNCH
                if (Input.GetButtonDown("Attack"))
                {
                    m_Character.Attack();
                }
                else if (Input.GetButtonUp("Attack"))
                {
                    m_Character.ClearAttack();
                }
                
                // LOCKON
                if (Input.GetButtonDown("LockOn"))
                {
                    m_Character.LockOn();
                }
                else if (Input.GetButtonUp("LockOn"))
                {
                    m_Character.LockOff();
                }

                // BLOCK
                if (Input.GetButtonDown("Block"))
                {
                    m_Character.Block();
                }
                else if (Input.GetButtonUp("Block"))
                {
                    m_Character.ReleaseBlock();
                }
            }
#endif
        }
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        if (m_Character.isAlive) {
            float h = 0;
            float v = 0;

#if UNITY_EDITOR
            // READ INPUTS
            h += Input.GetAxis("Horizontal");
            v += Input.GetAxis("Vertical");
#endif

            Vector2 movementDelta = Vector3.zero;

            if (movementJoystick != null) {
                movementDelta = movementJoystick.GetDelta();
            }

            h += movementDelta.x;
            v += movementDelta.y;

            // bool crouch = Input.GetKey(KeyCode.C) || Input.GetButton("Crouch");
            bool crouch = crouchState;

            crouch = crouch || Input.GetKey(KeyCode.C) || Input.GetButton("Crouch");

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v * Vector3.forward + h * Vector3.right;
            }
#if !MOBILE_INPUT
		// walk speed multiplier
	    if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // PASS ALL PARAMETERS TO THE CHARACTER CONTROL SCRIPT
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
