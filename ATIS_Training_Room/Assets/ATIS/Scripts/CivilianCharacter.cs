﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivilianCharacter : MonoBehaviour {

    public Animator civilianAnimator;
    public Mesh undressedMesh;
    public Material undressedMaterial;

    public SkinnedMeshRenderer skinnedMeshRenderer;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void GetKnockedOut(){
        civilianAnimator.SetBool("IsKnockedOut",true);
    }

    public void Undress()
    {
        skinnedMeshRenderer.sharedMesh = undressedMesh;
        skinnedMeshRenderer.sharedMaterial = undressedMaterial;
    }
}
