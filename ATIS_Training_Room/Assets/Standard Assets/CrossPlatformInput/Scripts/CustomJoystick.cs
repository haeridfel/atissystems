﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CustomControls
{
    public class CustomJoystick : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public float lerpAmount = 0.15f;
        public float sensitivity = 0.01f;

        private RectTransform rectTransform;
        private bool isRestoring = false;

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        void Update()
        {
            if (isRestoring)
            {
                rectTransform.transform.localPosition = Vector2.Lerp(
                                                                        rectTransform.transform.localPosition,
                                                                        Vector2.zero,
                                                                        lerpAmount
                                                                    );
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            rectTransform.transform.Translate(eventData.delta);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            isRestoring = true;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            isRestoring = false;
        }

        public Vector2 GetDelta()
        {
            return sensitivity * rectTransform.transform.localPosition;
        }
    }
}